# ToGo App

---

## Installation

## Server app

Before configuring API app make sure you have using PHP >= 7.1.3 you can find more info in [Laravel](https://laravel.com/docs/5.8) doc

For run API server you need to do
1. `cd api`
2. Copy .env.example to root dir and rename .env
3. `composer install`
4. `php artisan key:generate`
5. Setup your db configurations in .env file `DB_DATABASE`, `DB_USERNAME` and `DB_PASSWORD`
6. `php artisan migrate` for creating tables
7. `php artisan passport:install` for creation Client API key
8. You need to add client app URL to `APP_FRONT_URL` in .env file 
9. `php artisan serve` for starting php API server "default http://127.0.0.1:8000" also you need to change API url in .env file *for example* `APP_URL=http://127.0.0.1:8000`


##Configuring Mail
For mail sending for example "Forgot password" you need to set mail configurations
in *.env* file

`MAIL_DRIVER=smtp`

`MAIL_HOST=smtp.mailtrap.io`
 
`MAIL_PORT=2525`
 
`MAIL_USERNAME=null`
 
`MAIL_PASSWORD=null`
 
`MAIL_ENCRYPTION=null` 


## Client app
Before configuring client app make sure you have [Node.js](https://nodejs.org/en/) in your machine 


1. `cd client-app`
2. `npm install`
3. Copy .env.example to root dir and rename .env
4. set api URL in .env file `VUE_APP_API_BASE_URL` Laravel (default http://127.0.0.1:8000/api/v1)
5. We using Google Maps (places) API, You need to generate your own API key and set it in .env config file `VUE_APP_GOOGLE_API_KEY`
6. `npm run serve`
