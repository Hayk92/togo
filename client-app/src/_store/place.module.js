export default {
    state : {
        places: [],
        filterByMarkers : 'all'
    },
    mutations: {
        places(state, payload) {
            state.places = payload;
        },
        createPlace(state, payload) {
            payload.visited = false;
            state.places.push(payload);
        },
        removePlace(state, payload){
            let index = state.places.findIndex(place => place.id === payload);
            state.places.splice(index, 1)
        },
        placeVisited(state, payload) {
            let index = state.places.findIndex(place => {return place.id === payload;});
            state.places[index].visited = true;
        },
        markerChange(state, type) {
            state.filterByMarkers = type;
        }
    },
    actions: {
        places({dispatch, commit}, places) {
            commit('places', places);
        },
        createPlace({dispatch, commit}, data) {
            commit('createPlace', data)
        },
        removePlace({dispatch, commit}, id) {
            commit('removePlace', id);
        },
        placeVisited({dispatch, commit}, id) {
            commit('placeVisited', id);
        },
        changeMarkers({dispatch, commit}, type) {
            commit('markerChange', type);
        }
    },
    getters: {
        PLACES: state => {
            return state.places;
        },
        filterMarkerBy: state => state.filterByMarkers
    }
}
