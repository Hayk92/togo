const user = JSON.parse(localStorage.getItem('tg-app-user'));
const token = localStorage.getItem('tg-app-token');
const initialState = user
    ? { status: {loggedIn: !!token}, user, errors: {} }
    : { status: {}, user: null, errors: {}};

export default {
    state: initialState,
    actions: {
        login({dispatch, commit}, user) {
            commit('login', user);
            commit('loginSuccess', user);
        },
        register({dispatch, commit}, user) {
            commit('login', user);
            commit('registerSuccess', user);
        },
        logout({commit}) {
            commit('logout');
        },
        forgotPassword({commit}, email) {

        },
        resetPassword({commit}, user) {
            commit('loginSuccess', user);
        }
    },
    mutations: {
        login(state, user) {
            state.status = {loggingIn: true};
            state.user = user;
        },
        loginSuccess(state, user) {
            state.status = {loggedIn: true};
            state.user = user;
        },
        loginFail(state, err) {
            state.status = {loggedIn: false};
            state.user = null;
        },
        registerSuccess(state, user) {
            state.status = {loggedIn: true};
            state.user = user;
        },
        registerFail(state, err) {
            state.status = {};
            state.user = null;
        },
        logout(state) {
            state.status = {};
            state.user = null;
        }
    },
    getters : {
        USER: (state) => {
            return state.user;
        },
        STATUS: (state) => {
            return state.status;
        }
    }
};
