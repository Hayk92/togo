export default {
    state: {
        errors: {}
    },
    mutations: {
        errors(state, payload) {
            state.errors = payload;
        }
    },
    actions: {
        errors({dispatch, commit}, err) {;
            commit('errors', err);
        }
    },
    getters: {
        ERRORS: (state) => {
            return state.errors;
        }
    }
}
