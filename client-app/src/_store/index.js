import Vue from 'vue';
import Vuex from 'vuex';

import authentication from "./auth.module";
import place from "./place.module";
import errors from "./errors.module";

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
       auth : authentication,
       place: place,
       errors: errors
    }
});
