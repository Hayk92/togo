import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import { store } from './_store'
import './registerServiceWorker'
import VeeValidate from 'vee-validate';
import Vuetify from 'vuetify';
import * as VueGoogleMaps from 'vue2-google-maps'

import { responseErr } from "./mixins";

Vue.config.productionTip = false;

Vue.use(VeeValidate);
Vue.use(Vuetify);

Vue.use(VueGoogleMaps, {
    load : {
        key: process.env.VUE_APP_GOOGLE_API_KEY,
        libraries: 'places'
    }
});

Vue.mixin(responseErr);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
