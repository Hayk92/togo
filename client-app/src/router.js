import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import ForgotPassword from './views/ForgotPassword.vue'
import ResetPassword from './views/ResetPassword.vue'
import NotFound from './views/NotFound.vue'

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
          requiresAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
          guest: true
      }
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: {
          guest: true
      }
    },
    {
      path: '/forgot-password',
      name: 'forgot_password',
      component: ForgotPassword,
      meta: {
          guest: true
      }
    },
    {
      path: '/password-reset/:token',
      name: 'reset_password',
      component: ResetPassword,
      meta: {
          guest: true
      }
    },
    {
      path: '/page-not-found',
      name: 'page_not_found',
      component: NotFound
    },


    { path: '*', redirect: '/page-not-found' }
  ]
});

router.beforeEach((to, from, next) => {
    const publicPages = ['login', 'register', 'forgot_password', 'reset_password'];

    const authRequired = !publicPages.includes(to.name);
    const loggedIn = localStorage.getItem('tg-app-token');
    if (authRequired && !loggedIn) {
        return router.push('/login');
    }
    next();
});

export default router;
