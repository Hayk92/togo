import axios from 'axios'

let token = localStorage.getItem('tg-app-token');
const HTTP = axios.create({
    baseURL: process.env.VUE_APP_API_BASE_URL,
});

HTTP.interceptors.request.use(
    config => {
        let token = localStorage.getItem('tg-app-token');
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    },
    error => Promise.reject(error)
);

export default HTTP;
