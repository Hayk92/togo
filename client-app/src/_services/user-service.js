import  HTTP  from  "../http-common";

const login = (data) => {
    return HTTP.post(`/login`, data);
};

const logout = () => {
    return HTTP.post(`/logout`);
};

const register = (data) => {
    return HTTP.post(`/register`, data);
};

const forgotPassword = (email) => {
    return HTTP.post(`/password/email`, email);
};

const passwordReset = (data) => {
    return HTTP.post(`/password/reset`, data);
};

export const USER_SERVICE = {
    login,
    logout,
    register,
    forgotPassword,
    passwordReset
};
