import  HTTP  from '../http-common';

const allPlaces = () => {
    return HTTP.get(`/places`);
};

const addPlace = (data) => {
    return HTTP.post(`/places`, data)
};

const visited = (id) => {
    return HTTP.put(`/places/${id}`, {
        params : {
            id: id
        }
    })
};

const removePlace = (id) => {
    return HTTP.delete(`/places/${id}`, {
        params : {
            id: id
        }
    })
};

export const PLACES_SERVICE = {
    allPlaces,
    addPlace,
    visited,
    removePlace,
};
