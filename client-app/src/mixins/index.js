export const responseErr = {
    methods: {
        showErrors(err, status) {
            if (status === 422) {
                Object.keys(err).map((key) => {this.errors.add({field: key, msg: err[key]})});
            }
            if (status === 401) {
                localStorage.removeItem('tg-app-user');
                localStorage.removeItem('tg-app-token');
                this.$store.dispatch('logout');
                this.$router.push('/login');
            }
        }
    },
    computed : {
        places () {
            return this.$store.getters.PLACES || [];
        },
        user() {
            return this.$store.getters.USER;
        },
        loggedIn () {
            return this.$store.getters.STATUS.loggedIn;
        },
        filterMarkerBy () {
            return this.$store.getters.filterMarkerBy;
        }
    }
};
