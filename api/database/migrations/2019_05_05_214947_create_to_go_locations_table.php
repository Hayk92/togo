<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToGoLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('to_go_locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->string('name');
            $table->text('description');
	        $table->decimal('lng', 11, 8);
	        $table->decimal('lat', 10, 8);
	        $table->boolean('visited')->default(false);
            $table->timestamps();

            $table->foreign('user_id')->references('id')
	            ->on('users')
	            ->onUpdate('cascade')
	            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('to_go_locations');
    }
}
