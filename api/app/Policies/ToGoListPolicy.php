<?php

namespace App\Policies;

use App\Models\User;
use App\Models\ToGoLocation;
use Illuminate\Auth\Access\HandlesAuthorization;

class ToGoListPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the to go location.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ToGoLocation  $toGoLocation
     * @return mixed
     */
    public function view(User $user, ToGoLocation $toGoLocation)
    {
        return $user->id === $toGoLocation->user_id;
    }

    /**
     * Determine whether the user can create to go locations.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the to go location.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ToGoLocation  $toGoLocation
     * @return mixed
     */
    public function update(User $user, ToGoLocation $toGoLocation)
    {
	    return $user->id === $toGoLocation->user_id;
    }

    /**
     * Determine whether the user can delete the to go location.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ToGoLocation  $toGoLocation
     * @return mixed
     */
    public function delete(User $user, ToGoLocation $toGoLocation)
    {
	    return $user->id === $toGoLocation->user_id;
    }

    /**
     * Determine whether the user can restore the to go location.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ToGoLocation  $toGoLocation
     * @return mixed
     */
    public function restore(User $user, ToGoLocation $toGoLocation)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the to go location.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ToGoLocation  $toGoLocation
     * @return mixed
     */
    public function forceDelete(User $user, ToGoLocation $toGoLocation)
    {
        //
    }
}
