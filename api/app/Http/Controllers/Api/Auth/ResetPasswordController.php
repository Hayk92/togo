<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

	public function reset(Request $request)
	{
		$request->validate($this->rules(), $this->validationErrorMessages());

		$response = $this->broker()->reset(
			$this->credentials($request), function ($user, $password) {
			$this->resetPassword($user, $password);
		});

		$user = Auth::user();
		if ($response == Password::PASSWORD_RESET){
			$success['token'] = $user->createToken(env('APP_NAME'))->accessToken;
			$success['user'] = $user;
			return response()->json(['success' => $success], 200);
		}else {
			return response()->json(['errors' => ['email' => trans($response)]], 422);
		}
	}
}
