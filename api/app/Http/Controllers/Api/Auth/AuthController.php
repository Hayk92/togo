<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Traits\Auth\AuthUser;

class AuthController extends Controller
{
    use AuthUser;

}
