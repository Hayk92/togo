<?php
namespace App\Traits\Auth;

use App\Http\Requests\UserLogin;
use App\Http\Requests\UserRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

trait AuthUser
{
	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function register(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'required|confirmed',
		]);

		if ($validator->fails()) {
			return response()->json(['errors' => $validator->errors()], 422);
		}
		$data = $request->all();
		$data['password'] = bcrypt($data['password']);
		$user = User::create($data);
		Auth::attempt(['email' => $data['email'], 'password' => $data['password']]);
		$success['token'] = $user->createToken(env('APP_NAME'))->accessToken;
		$success['user'] = $user;
		return response()->json(['success' => $success], 200);
	}

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function login(UserLogin $request)
	{
		$credentials = $request->only('email', 'password');
		if (Auth::attempt($credentials, $request->filled('remember'))){
			$user = Auth::user();
			$success['token'] = $user->createToken(env('APP_NAME'))->accessToken;
			$success['user'] = $user;
			return response()->json(['success' => $success], 200);
		}else{
			return response()->json(['errors' => ['email' => 'The user credentials is invalid!']], 422);
		}
	}

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function logout()
	{
		Auth::user()->tokens()->delete();
		return response()->json(['success' => 'Successfully logged out!'], 200);
	}

	/**
	 * @param ResetPasswordRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function resetPassword(ResetPasswordRequest $request)
	{
		$user = User::query()->where('email', $request->email)->firstOrFail();
		$user->password = bcrypt($request->password);
		$user->save();
		$success['token'] = $user->createToken(env('APP_NAME'))->accessToken;
		$success['email'] = $user->email;
		return response()->json(['success' => $success], 200);
	}
}