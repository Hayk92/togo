<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ToGoLocation extends Model
{
    protected $fillable = [
	    'user_id',
		'name',
		'description',
		'lng',
		'lat',
		'visited',
    ];

    protected $appends = ['position', 'details'];

	public function user()
	{
		return $this->belongsTo(User::class, 'id', 'user_id');
	}

	public function getPositionAttribute()
	{
		return ['lat' => (float)$this->lat, 'lng' => (float)$this->lng];
	}

	public function getDetailsAttribute()
	{
		return false;
	}
}
