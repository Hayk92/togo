<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function(){
	Route::post('login', 'Api\Auth\AuthController@login');
	Route::post('register', 'Api\Auth\AuthController@register');
	Route::post('/password/email', 'Api\Auth\ForgotPasswordController');
	Route::post('/password/reset', 'Api\Auth\AuthController@resetPassword');

	Route::group(['middleware' => 'auth:api'], function(){
		Route::post('logout', 'Api\Auth\AuthController@logout');
		Route::resource('places', 'Api\PlaceListController');
	});
});